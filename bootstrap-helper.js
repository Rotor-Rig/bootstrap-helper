/*jshint globalstrict: true*/
/*jshint esversion: 6 */
/* jshint browser: true */
/* jshint devel: true */
/* jshint jquery: true */
/*jshint laxbreak: true */
"use strict";
//------------------------------------------standard bootstrap form controls ---------------------------------
//------seems to work best if it goes paneltop -- - panel body top -- formtop - -- the controls -- formbottom --panel body bottom -- panelbottom
//alerts work outside the form
//back/next buttons work outside of the panel
/*
function example_UI () {
	var pagecontent = stdPageStart();
	pagecontent += stdBackButton ('drawRaceGrid()');
	pagecontent += stdPanelTop('Select Race');
	pagecontent += stdPanelBodyTop ();
	pagecontent += stdFormTop();
	pagecontent += stdEmptyDropBox ('Race', 'col-sm-1', 'col-sm-4', 'select-dropbox-choices');
	pagecontent += stdFormBottom ();
	pagecontent += stdAlert ('Problem', 'select-race-alert');
	pagecontent += stdPanelBodyBottom ();
	pagecontent += stdPanelBottom();
	pagecontent += stdNextButton ('setSelectedRace($(\'#select-race-choices\').val())', 'Continue', '');
	pagecontent += stdPageEnd();
	$('#pageID').empty();
	$('#pageID').append(pagecontent);

	//populate the dropbox choices with a function that returns the list
	raceEntryPageEvents().then(function(info) {
		$('#select-dropbox-choices').empty();
		$('#select-dropbox-choices').append(info.page);
	});
}
*/
//export default /*function bs_h()*/ {

var action_icon = "<i class=\"fas fa-chevron-right fa-lg ml-2 align-middle\"></i>";
var action_icon_reverse = "<i class=\"fas fa-chevron-left fa-lg mr-2 align-middle\"></i>";
var delete_icon = "<i class=\"fas fa-trash-alt\"></i>";

//"action_icon": "<i class=\"fas fa-chevron-right fa-lg ml-2 align-middle\"></i>",
//"action_icon_reverse": "<i class=\"fas fa-chevron-left fa-lg mr-2 align-middle\"></i>",
//"delete_icon": "<i class=\"fas fa-trash-alt\"></i>",

function setActionIcon (i) {
	action_icon = i;
}
function setActionIconReverse (i) {
	action_icon_reverse = i;
}

function setDeleteIcon(i) {
	delete_icon = i;
}
/*
function safeString(s) {
  //from https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
  var specials = [
      // order matters for these
        "-"
      , "["
      , "]"
      // order doesn't matter for any of these
      , "/"
      , "{"
      , "}"
      , "("
      , ")"
      , "*"
      , "+"
      , "?"
      , "."
      , "\\"
      , "^"
      , "$"
      , "|"
			, "\""
    ];
    return s.replace(RegExp('[' + specials.join('\\') + ']', 'g'), "\\$&");
}
*/
function safeString(s) {
	return s.replace(/"/g, "&quot;");
}
function stdPageStart() {
//function PageStart() {
	return "<div class='container-fluid'>";
}
function stdRowStart(opt) {
	if (!opt)	return "<div class='row'>";
	if (opt =="form") return "<div class=\"form-row\">";
	else return "<div class=\"row " + opt + "\">";
}
function stdColumnStart (colwidth) {
	var w = "col";
	if (colwidth) w = colwidth;
	return "<div class=\"" + w + "\">";
}
function stdFormTop(inline, option_classes, id_in, allow_submit) {
	var txt = "<form onsubmit=\"return false;\"";
	if (allow_submit) txt = "<form";
	var opt_cl = "";
	var id = "";
	if (id_in) {
		id = " id=\"" + id_in + "\"";
		txt += id;
	}
	if (option_classes) opt_cl = option_classes;
	if (inline)	txt += " class=\"form-inline " + opt_cl + "\"";
	else if (option_classes) txt += " class=\"" + opt_cl + "\"";
	txt += ">";
	return txt;
}
function stdFormBottom () {return "</form>";}

//-----add a forward arrow if action defined -- material icon keyboard_arrow_right
function stdListItem (title, id, extra_classes, options, badge_value, action, special) {
	if (id) id = "id='" + id + "' ";
	else id = "";
	var classes = "";
	if (extra_classes) classes = " " + extra_classes;

	if (options) options = " " + options;
	else options = "";

	var badge_def = "";
	if (!isNaN(badge_value)) badge_def = "<span class=\"badge badge-info badge-pill\">" + badge_value + "</span>";

	var click_action = "";
	if (action) click_action = " onclick=\"" + action + "\"";

	var the_icon = "";
	if (action) {
		if(special =="reverse")	{
			the_icon = action_icon_reverse;
		}
		else {
			the_icon = action_icon;
		}
	}

	var the_area = "";
	if (the_icon || badge_def) {
		if (special =="reverse") the_area = "<span class='float-left mr-1'>" + the_icon + badge_def + "</span>";
		else the_area = "<span class='float-right ml-1'>" + badge_def + the_icon + "</span>";
	}

	var pg = "<button type=\"button\" " + id + "class=\"list-group-item list-group-item-action" + classes + "\"" + click_action +
	 options + ">";
	 if (special == "reverse") pg += the_area + title + "</button>";
	 else pg += title + the_area + "</button>";

	//console.log("list item: " + pg);

	return pg;
}
function stdListStart(id, c) {
	if (id) id = "id=\"" + id + "\" ";
	else id = "";
	var c_txt = "";
	if (c) c_txt = " " + c;
	return "<div " + id + "class=\"list-group" + c_txt + "\">";
}
function divEnd() {
	return "</div>";
}
//TODO fix input in timer
function stdInput (label, option_classes, id, options, label_options, value, num_rows, txt) {
	if (!options) options = "";
	else options = " " + options;
	if (!value) value = "";
	else value = " value=\"" + safeString(value.toString()) + "\"";
	if (!label_options) label_options = "";
	var input_html = "<div class=\"form-group " + option_classes + "\">";
	if (label) input_html += "<label class=\"" + label_options + "\" for=\"" + id + "\">" + label + " </label>";
	if (num_rows) input_html += "<textarea class=\"form-control\" id=\"" + id + "\"" + options + " rows=\"" + num_rows + "\">" + safeString(txt) + "</textarea></div>";
	else input_html += "<input class=\"form-control\" id=\"" + id + "\"" + options + value + "></div>";
	return input_html;
}
function stdNoLabelInput(txt, option_classes, options, num_rows) {
	if (option_classes) option_classes = " " + option_classes;
	else option_classes = "";
	if (options) options = " " + options;
	else options = "";
	var the_html = "";
	if (num_rows) the_html = "<textarea class=\"form-control" + option_classes + "\" rows=\"" + num_rows + "\"" + options + ">" + safeString(txt) + "</textarea>";
	else the_html = "<input class=\"form-control" + option_classes + "\" type=\"text\" placeholder=\"" + safeString(txt) + "\"" + options + ">";
	return the_html;
}
function stdInputButton (txt, input_opt, action, opt, btn_title, btn_opt) {
	if (!txt) txt = "";
	if (!input_opt) input_opt = "type=\"text\"";
	if (!action) return;
	if (!opt) opt = "";
	if (!btn_title) btn_title = "";
	if (!btn_opt) btn_opt = "btn-info";
	var b = "";
	b += "<div class=\"input-group " + opt + "\">";
	b += "<div class=\"input-group-prepend\">";
	b += "<span class=\"input-group-text\">" + txt + "</span></div>";
	b += "<input " + input_opt + " class=\"form-control\">";
	b += "<div class=\"input-group-append\">";
	b += "<button onclick =\"" + action + "\" class=\"btn " + btn_opt + "\" type=\"button\">" + btn_title + "</button></div></div>";
	//b += "<button class=\"btn " + btn_opt + "\" type=\"button\">" + btn_title + "</button></div></div>";
	return b;
}
function stdButton (title, action, option_classes, id) {
	if (!option_classes) option_classes = " btn-primary";
	else option_classes = " " + option_classes;
	var b = "<button type=\"button\" class=\"btn" + option_classes + "\" ";
	if (id) b += "id=\"" + id + "\" ";
	b += "onclick=\"" + action + "\">" + title + "</button>";
	//b += ">" + title + "</button>";
	return b;
}

function stdDeleteButton(c, f, id) {
	var c_txt = "";
	var f_txt = "";
	var id_txt = "";
	if (c) c_txt = " class=\"" + c + "\" ";
	if (f) f_txt = " onclick=\"" + f + "\" ";
	if (id) id_txt = " id=\"" + id + "\"";
	return "<button" + id_txt + " type=\"button\"" + c_txt + f_txt + ">" + delete_icon + "</button>";
}
function stdSwitch (title, id, enabled, f, labelclass, option_classes) {
	if (f) f = " onchange=\"" + f + "\"";
	else f = "";
	if (labelclass) labelclass = " " + labelclass;
	else labelclass = "";
	if (enabled) enabled = " checked";
	else enabled = "";
	if (option_classes) option_classes = " class=\"" + option_classes + "\"";
	else option_classes = "";
	var b = "<small><span id=\"" + id + "-text\">" + title + "</span></small>";
	b += "<label class=\"switch" + labelclass + "\"><input id=\"" + id + "\" type=\"checkbox\"" + option_classes + enabled + f + ">";
	b += "<span class=\"slider\"></span></label>";
	return b;
	//depends on some specialised CSS for switch and slider
	/*
	<small><span id="finish-switch-text" class="text-light mr-1">Finish Mode</span></small>
	<label class="switch">
			<input id="finish-switch" type="checkbox" onchange="finishSwitch()">
			<span class="slider"></span>
	</label>
	*/
}
function stdSwitch2 (title, id, enabled, f, labelclass, option_classes, titleclass) {
	if (f) f = " onchange=\"" + f + "\"";
	else f = "";
	if (labelclass) labelclass = " " + labelclass;
	else labelclass = "";
	if (titleclass) titleclass = " " + titleclass;
	else titleclass = "";
	if (enabled) enabled = " checked";
	else enabled = "";
	if (option_classes) option_classes = " class=\"" + option_classes + "\"";
	else option_classes = "";
	var t = "<span class=\"" + titleclass + "\" id=\"" + id + "-text\">" + title + "</span>";
	var b = "<label class=\"switch" + labelclass + "\"><input id=\"" + id + "\" type=\"checkbox\"" + option_classes + enabled + f + ">";
	b += "<span class=\"slider\"></span></label>";
	b += t;
	return b;
	//depends on some specialised CSS for switch and slider
	/*
	input switch Bootstrap 4.0.0 Snippet by prakash27dec
.switch input {
    display:none;
}
.switch {
    display:inline-block;
    width:40px;
    height:20px;
    margin:6px;
    position:relative;
}

.slider {
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    right:0;
    border-radius:20px;
    box-shadow:0 0 0 2px #777, 0 0 4px #777;
    cursor:pointer;
    border:4px solid transparent;
    overflow:hidden;
     transition:.1s;
}
.slider:before {
    position:absolute;
    content:"";
    width:100%;
    height:100%;
    background:#777;
    border-radius:20px;
    transform:translateX(-20px);
    transition:.1s;
}
input:checked + .slider:before {
    transform:translateX(20px);
    background:limeGreen;
}
input:checked + .slider {
    box-shadow:0 0 0 2px limeGreen,0 0 2px limeGreen;
}
	*/
}
function stdCheckbox (label, id, enabled, f, labelclass, option_classes) {
	var ticked = "";
	if (enabled) ticked = " checked ";
	var action = "";
	//if (f) action = " onchange=\"" + f + "\" ";
	if (f) action = " onclick=\"" + f + "\" ";
	if (!labelclass) labelclass = "";
	if (!option_classes) option_classes = "form-check";
	return "<div class=\"" + option_classes + "\"><label class=\"form-check-label " + labelclass + "\"><input class=\"form-check-input\" type=\"checkbox\" id=\"" + id + "\"" + ticked + action + ">" + label + "</label></div>";
}
	//add checkbox to show advanced sync buttons like this div class="checkbox"><label><input type="checkbox">Advanced Options</label></div>
	//stdCheckbox(label)
//a list of radio values---------------------------------
function stdRadioOptions (value_list, text_list, def, id, options, label_classes, defval) {
	if (!text_list) text_list = value_list;
	if (!label_classes) label_classes = "";
	if (!options) options = "";
	else options = " " + options;
	var r = "";
	for (var val in value_list) {
		var chk = "";
		if (def == text_list[val]) chk = " checked ";
		if (defval === value_list[val]) chk = " checked ";
		//console.log("defval is " + defval + " and valuelist is " + value_list[val]);
		r += "<div class=\"form-check form-check-inline" + options + "\">";
		r += "<input type=\"radio\" name=\"" + id + "\" value=\"" + value_list[val] + "\"" + chk + " id=\"" + id + val +"\" class=\"form-check-input\">";
		r += "<label for=\"" + id + val + "\" class=\"form-check-label " + label_classes + "\">" + safeString(text_list[val]) + "</label>";
		r += "</div>";
	}
	//r += "</div>";
	return r;
}
function stdAlert (message, id, visible, xme, option_classes) {
	var c_opt = " my-2 alert-danger";
	if (option_classes) c_opt = " " + option_classes;
	if (id) id = "id=\"" + id + "\"";
	else id = "";
	var collapse = " collapse";
	var dismiss = "<button type=\"button\" class=\"pl-2 close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
		"<span aria-hidden=\"true\">&times;</span></button>";
	if (!xme) dismiss = "";
	if (visible) collapse = "";
	return "<div " + id + "class=\"alert alert-dismissable" + collapse + c_opt + "\">" + message
		+ dismiss + "</div>";
}
function stdInfo (message, id, hide) {
	var c = "";
	if (hide) c = " style=\"display:none;\" ";
	var p = "<div id=\"" + id + "\" class=\"alert alert-info\" " + c + " role=\"alert\">";
	p += "<span id=\"" + id + "-msg\">" + message + "</span>";
	p += "<button type=\"button\" class=\"close\" onclick=\"$('#" + id + "').hide()\" aria-label=\"Close\">";
	//p += "<button type=\"button\" class=\"close\" aria-label=\"Close\">";
   p += "<span aria-hidden=\"true\">&times;</span></button>";
	p += "</div>";
	return p;
}
function stdDropBox (label, width, id, options, values) {
	//console.log("values : " + JSON.stringify(values));
	if (!options) options = "";
	else options = " " + options;
	var bx = "<div class=\"form-group " + width + "\"><label for=\"" + id +
	"\" class=\"texthighlight\">" + label + "</label>"+
	"<select name=\"" + id + "\" id=\"" + id + "\" class=\"form-control\"" + options + ">";
	if (values) for (var v in values) {
		var selected = "";
		if (values[v].length > 2) {
			if (values[v][2]) selected = " selected";
		}
		var value_txt = "";
		//console.log("dropbox value is " + safeString(values[v][0]));
		if (values[v][0]) value_txt = " value=\"" + safeString(values[v][0]) + "\"";
		bx += "<option" + value_txt + selected + ">" + values[v][1] + "</option>";
	}
	bx += "</select></div>";
	return bx;
}
function stdSpacer (i) {
	if (!i) i = 1;
	return "<div style=\"margin:0; padding:0; height:" + i + "px\"></div>";
}
/////////////
/////////////
/////////////
//v4-------------------
function stdText(t, c, extra_classes, id) {
	if (!c) c = "text-info";
	else if (c == "blue") c = "text-primary";
	else if (c == "green") c = "text-success";
	else if (c == "red") c = "text-danger";
	else if (c == "black") c = "text-dark";
	else if (c == "white") c = "text-white";
	else if (c == "grey") c = "text-secondary";
	else if (c == "amber") c = "text-warning";
	else c = "text-info";
	var p = "<span class=\"";
	p += c;
	if (extra_classes) p += " " + extra_classes + "\"";
	else p += "\"";
	var id_txt = "";
	if (id) {
		id_txt = " id=\"" + id + "\"";
		p += id_txt;
	}
	p += ">" + t + "</span>";
	return p;
}
function stdParagraph(t, c, extra_classes, id) {
	if (!c) c = "text-info";
	else if (c == "blue") c = "text-primary";
	else if (c == "green") c = "text-success";
	else if (c == "red") c = "text-danger";
	else if (c == "black") c = "text-dark";
	else if (c == "white") c = "text-white";
	else if (c == "grey") c = "text-secondary";
	else if (c == "amber") c = "text-warning";
	else c = "text-info";
	var p = "<p class=\"";
	p += c;
	if (extra_classes) p += " " + extra_classes + "\"";
	else p += "\"";
	var id_txt = "";
	if (id) {
		id_txt = " id=\"" + id + "\"";
		p += id_txt;
	}
	p += ">" + t + "</p>";
	return p;
}
function stdBadge(t, c, opt, id) {
	if (!c) c = "badge-info";
	else if (c == "blue") c = "badge-primary";
	else if (c == "green") c = "badge-success";
	else if (c == "red") c = "badge-danger";
	else if (c == "black") c = "badge-dark";
	else if (c == "white") c = "badge-light";
	else if (c == "grey") c = "badge-secondary";
	else if (c == "amber") c = "badge-warning";
	else c = "badge-info";
	var p = "<span class=\"badge ";
	p += c;
	if (opt) p += " " + opt + "\"";
	if (id) p += " id=\"" + id + "\"";
	p += ">" + t + "</span>";
	return p;
}
function stdPanelBodyTop (options, title, txt, class_option) {
	var opt = "";
	if (options) opt = " " + options;
	if (class_option) class_option = " " + class_option;
	else class_option = "";
	var r = "<div class=\"card-body bg-transparent" + class_option + "\"" + opt + ">";
	if (title) r += "<h6 class=\"card-title\">" + title + "</h6>";
	if (txt) r += "<p class=\"card-text\">" + txt + "</p>";
	return r;
}
//-----------------
function stdPanelTop(title, options, class_option, panel_type) {
	var opt = "";
	if (options) opt = " " + options;
	var c = "\"";
	if (class_option) c = " " + class_option + "\"";
	var panel_type_def = "";
	if (!panel_type) panel_type = "default";
	if (panel_type == "info") panel_type_def = "bg-info text-light h5";
	else if (panel_type == "default") panel_type_def = "bg-light text-dark h5";
	else if (panel_type == "danger") panel_type_def = "bg-danger text-light h5";
	else panel_type_def = panel_type;
	return "<div class=\"card" + c + " " + opt + "><div class=\"card-header " + panel_type_def + "\">" + title + "</div>";
}
function stdTableTop() {return "<div class=\"d-flex flex-row flex-sm-wrap\">";}
function stdTableRowStart() {return "<div class=\"d-flex flex-row flex-sm-wrap\">";}
function stdTableColStart() {return "<div class=\"d-flex flex-column\">";}
function stdCard(cd_classes, cd_options) {
	var c = "<div class=\"card";
	if (cd_classes) c += " " + cd_classes;
	c += "\"";
	if (cd_options) c += " " + cd_options;
	c += ">";
	return c;
}
function stdCardBody (cd_bodyclasses, cd_bodyoptions) {
	var c = "";
	if (!cd_bodyclasses) cd_bodyclasses = "";
	if (!cd_bodyoptions) cd_bodyoptions = "";
	c += "<div class=\"card-body "+ cd_bodyclasses + "\" " + cd_bodyoptions + ">";
	return c;
}
function stdJumbo (j_content, j_classes, j_options) {
	var c = "";
	if (j_classes) j_classes = " " + j_classes;
	else j_classes = "";
	if (!j_options) j_options = "";
	c += "<div class=\"jumbotron"+ j_classes + "\" " + j_options + ">";
	if (j_content) c += j_content;
	return c;
}

//////////////
//////////////
/*
  return {
    PageStart as stdPageStart,
    PageEnd as divEnd,
    PanelTop as stdPanelTop,
    PanelBottom:divEnd,
    PanelBodyTop:stdPanelBodyTop,
    PanelBodyBottom:divEnd,
    RowStart:stdRowStart,
    RowEnd:divEnd,
    ColumnStart:stdColumnStart,
    ColumnEnd:divEnd,
		ActionIcon: setActionIcon,
		ActionIconReverse: setActionIconReverse,
		DeleteIcon: setDeleteIcon,
    ListStart:stdListStart,
    ListEnd:divEnd,
    ListItem:stdListItem,
    Input:stdInput,
		NoLabelInput:stdNoLabelInput,
    FormTop:stdFormTop,
    FormBottom:stdFormBottom,
    Button:stdButton,
		DeleteButton: stdDeleteButton,
		InputButton:stdInputButton,
		Switch:stdSwitch,
    Checkbox:stdCheckbox,
    RadioOptions:stdRadioOptions,
    DropBox:stdDropBox,
    Alert:stdAlert,
    Info:stdInfo,
    Spacer:stdSpacer,
    Text: stdText,
		Paragraph: stdParagraph,
    Badge: stdBadge,
    TableTop: stdTableTop,
    TableBottom: divEnd,
    TableRowStart: stdTableRowStart,
    TableRowEnd: divEnd,
    TableColStart: stdTableColStart,
    TableColEnd: divEnd,
    Card: stdCard,
		CardEnd: divEnd,
		CardBody: stdCardBody,
		CardBodyEnd: divEnd,
		Jumbo: stdJumbo,
		JumboEnd: divEnd
  };
*/
export {
	stdPageStart as PageStart,
	stdPanelTop as PanelTop,
	stdPanelBodyTop as PanelBodyTop,
	stdRowStart as RowStart,
	divEnd as RowEnd,
	stdColumnStart as ColumnStart,
	divEnd as ColumnEnd,
	setActionIcon,
	setActionIconReverse,
	setDeleteIcon,
	stdListStart as ListStart,
	stdListItem as ListItem,
	stdInput as Input,
	stdNoLabelInput as NoLabelInput,
	stdFormTop as FormTop,
	stdFormBottom as FormBottom,
	stdButton as Button,
	stdDeleteButton as DeleteButton,
	stdInputButton as InputButton,
	stdSwitch as Switch,
	stdSwitch2 as Switch2,
	stdCheckbox as Checkbox,
	stdRadioOptions as RadioOptions,
	stdDropBox as DropBox,
	stdAlert as Alert,
	stdInfo as Info,
	stdSpacer as Spacer,
	stdText as Text,
	stdParagraph as Paragraph,
	stdBadge as Badge,
	stdTableTop as TableTop,
	stdTableRowStart as TableRowStart,
	stdTableColStart as TableColStart,
	stdCard as Card,
	stdCardBody as CardBody,
	stdJumbo as Jumbo,
	divEnd as CardBodyEnd,
	divEnd as CardEnd,
	divEnd as TableColEnd,
	divEnd as TableRowEnd,
	divEnd as TableBottom,
	divEnd as ListEnd,
	divEnd as PanelBodyBottom,
	divEnd as PanelBottom,
	divEnd as PageEnd,
	divEnd as JumboEnd
};
//}
//-------------------------END-------------------------------------------------------------------------------------------------------
