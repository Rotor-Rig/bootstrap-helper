# bootstrap-helper

This is a simple library to help me layout Bootstrap 4 pages from JS code. I found it difficult to get all the "<div>"s in the right order. Some of this is not tested properly and it may have a number of bugs, but please use if it helps you.

This is an example function that produces a clickable list in a Bootstrap 4 grid row:

```javascript
import * as bs_h from './bootstrap-helper.js';
function testList () {
	var p = "";
	p += bs_h.PageStart();
	p += bs_h.RowStart();
	p += bs_h.ColumnStart("col-md-6");
	p += bs_h.ListStart('list_id');
	p += bs_h.ListItem('List Item 1', 'list_item_id1', '', '', 10, 'alert(\'click\')');
	p += bs_h.ListItem('List Item 2', 'list_item_id2', '', '', 20, 'alert(\'click\')');
	p += bs_h.ListItem('List Item 3', 'list_item_id3', '', '', 30, 'alert(\'click\')');
	p += bs_h.ListEnd();
	p += bs_h.ColumnEnd();
	p += bs_h.RowEnd();
	p += bs_h.PageEnd();
	var e = document.getElementById("main");
  e.innerHTML = p;
}
```

Copyright Ian Cherrill 2018-20

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
